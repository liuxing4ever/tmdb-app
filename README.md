# Tmdb App

This project is a simple Android app displaying an infinite list of the most popular movies on [TMDB.org](https://www.themoviedb.org/), and offering the possibility to search movies by keyword

## Popular movies

As soon as the user scrolls down and reaches the end of the list, the next page of movies is loaded

![Popular](https://www.dropbox.com/s/rxczr7uin1cl750/popular_movies.gif?dl=1) 

## Search as you type

The other functionality is the search. The user can search for a movie by keyword:

- The search is automatically triggered while typing the search term
- In the search results the movie **title**, **year** of release, **overview** without truncation and a **picture** are displayed

![Search](https://www.dropbox.com/s/55vv56ixr809url/search_as_you_type.gif?dl=1)

### Development Stack

- Kotlin
- Retrofit
- Dagger 2
- Picasso
- Junit, Mockito
- Google Architecture Components
    - ViewModel, PagedList, LiveData
	

### Possible improvements

- More test coverage
- Retry online query if it failed the first time
- Store query results in local database
