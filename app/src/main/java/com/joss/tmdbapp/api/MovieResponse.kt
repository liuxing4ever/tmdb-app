package com.joss.tmdbapp.api

import com.google.gson.annotations.SerializedName
import com.joss.tmdbapp.model.Movie

data class MovieResponse(
        @SerializedName("results") val results: List<Movie> = emptyList(),
        @SerializedName("page") val page: Int
)