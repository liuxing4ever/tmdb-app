package com.joss.tmdbapp.api


/**
 *     TMDB API-specific resources
 *    =============================
 */

const val apiKey = "93aea0c77bc168d8bbce3918cefefa45"

fun getImageUrlString(imgPath: String): String {
    return "https://image.tmdb.org/t/p/w500$imgPath"
}