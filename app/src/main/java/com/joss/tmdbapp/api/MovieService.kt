package com.joss.tmdbapp.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {

    @GET("movie/popular?api_key=$apiKey")
    fun getPopularMoviesPage(
            @Query("page") page: Int
    ): Call<MovieResponse>

    @GET("search/movie?api_key=$apiKey")
    fun getMoviesWithSearchQueryAtPage(
            @Query("query") query: String,
            @Query("page") page: Int
    ): Call<MovieResponse>

}