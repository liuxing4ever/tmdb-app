package com.joss.tmdbapp

import android.app.Application
import com.joss.tmdbapp.di.AppComponent
import com.joss.tmdbapp.di.DaggerAppComponent

class TmdbApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.create()
    }

}