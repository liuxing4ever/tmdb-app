package com.joss.tmdbapp.data

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class Listing<T>(
        val pagedList: LiveData<PagedList<T>>,
        val initialLoad: LiveData<NetworkState>)