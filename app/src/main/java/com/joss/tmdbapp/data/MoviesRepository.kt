package com.joss.tmdbapp.data

import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.support.annotation.MainThread
import com.joss.tmdbapp.api.MovieService
import com.joss.tmdbapp.model.Movie
import java.util.concurrent.Executor

class MoviesRepository(private val movieService: MovieService,
                       private val networkExecutor: Executor) {
    @MainThread
    fun getMovies(query: String): Listing<Movie> {
        val sourceFactory = MoviesDataSourceFactory(movieService, query)

        val config = PagedList.Config.Builder()
                .setPageSize(10)
                .setInitialLoadSizeHint(10)
                .setEnablePlaceholders(true)
                .build()

        val livePagedList = LivePagedListBuilder(sourceFactory, config)
                // provide custom executor for network requests, otherwise it will default to
                // Arch Components' IO pool which is also used for disk access
                .setFetchExecutor(networkExecutor)
                .build()

        return Listing(
                pagedList = livePagedList,
                initialLoad = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.initialLoad
                }
        )
    }
}