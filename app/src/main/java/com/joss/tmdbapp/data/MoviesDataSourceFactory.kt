package com.joss.tmdbapp.data

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.joss.tmdbapp.api.MovieService
import com.joss.tmdbapp.model.Movie

class MoviesDataSourceFactory(
        private val movieService: MovieService,
        private val query: String) : DataSource.Factory<Int, Movie>() {

    val sourceLiveData = MutableLiveData<MoviesDataSource>()

    override fun create(): DataSource<Int, Movie> {
        val source = MoviesDataSource(movieService, query)
        sourceLiveData.postValue(source)
        return source
    }
}