package com.joss.tmdbapp.data

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import android.util.Log
import com.joss.tmdbapp.api.MovieService
import com.joss.tmdbapp.api.MovieResponse
import com.joss.tmdbapp.model.Movie
import retrofit2.Call
import retrofit2.Response


class MoviesDataSource(private val movieService: MovieService,
                       private val query: String) : PageKeyedDataSource<Int, Movie>() {

    companion object {
        private const val TAG = "MoviesDataSource"
        const val INITIAL_LOAD_FAILURE = "initial load failure"
        private const val PAGE_1 = 1
    }

    private var loadCount = 0

    val initialLoad = MutableLiveData<NetworkState>()

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) { }

    override fun loadInitial(params: LoadInitialParams<Int>,
                             callback: LoadInitialCallback<Int, Movie>) {
        initialLoad.postValue(NetworkState.LOADING)

        val fromSearch = query.isNotEmpty()
        val moviesRequest = if (!fromSearch) movieService.getPopularMoviesPage(PAGE_1)
                            else movieService.getMoviesWithSearchQueryAtPage(query, PAGE_1)

        moviesRequest.enqueue(
                object : retrofit2.Callback<MovieResponse> {
                    override fun onFailure(call: Call<MovieResponse>?, t: Throwable?) {
                        handleInitialLoadError(INITIAL_LOAD_FAILURE)
                    }
                    override fun onResponse(call: Call<MovieResponse>?,
                                            response: Response<MovieResponse>?) {
                        when {
                            response == null -> handleInitialLoadError("null response")
                            response.isSuccessful -> {
                                Log.i(TAG, "initial load success")
                                val results = response.body()?.results ?: emptyList()
                                if (fromSearch) results.map { it.fromSearch = true }
                                callback.onResult(results, PAGE_1-1,PAGE_1+1)
                                initialLoad.postValue(NetworkState.LOADED)
                            }
                            else -> handleInitialLoadError("error code: ${response.code()}")
                        }
                    }
                })
    }

    override fun loadAfter(params: LoadParams<Int>,
                           callback: LoadCallback<Int, Movie>) {
        val page = params.key

        val fromSearch = query.isNotEmpty()
        val moviesRequest = if (!fromSearch) movieService.getPopularMoviesPage(page)
                            else movieService.getMoviesWithSearchQueryAtPage(query, page)

        moviesRequest.enqueue(
                object : retrofit2.Callback<MovieResponse> {
                    override fun onFailure(call: Call<MovieResponse>?, t: Throwable?) {
                        handleLoadError("load failure")
                    }
                    override fun onResponse(call: Call<MovieResponse>?,
                                            response: Response<MovieResponse>?) {
                        when {
                            response == null -> handleLoadError("null response")
                            response.isSuccessful -> {
                                Log.i(TAG, "load success")
                                val results = response.body()?.results ?: emptyList()
                                if (fromSearch) results.map { it.fromSearch = true }
                                callback.onResult(results, page+1)
                                initialLoad.postValue(NetworkState.LOADED)
                            }
                            else -> handleLoadError("error code: ${response.code()}")
                        }
                    }
                })

        loadCount ++
        Log.d(TAG, "loadAfter #$loadCount")
    }

    private fun handleInitialLoadError(msg: String) {
        Log.w(TAG, msg)
        initialLoad.postValue(NetworkState.error(msg))
    }

    private fun handleLoadError(msg: String) {
        Log.w(TAG, msg)
        initialLoad.postValue(NetworkState.error(msg))
    }
}