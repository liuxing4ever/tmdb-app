package com.joss.tmdbapp.di

import com.joss.tmdbapp.ui.MainActivity
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ViewModelModule::class,
    NetModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}