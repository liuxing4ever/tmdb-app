package com.joss.tmdbapp.di

import com.joss.tmdbapp.api.MovieService
import com.joss.tmdbapp.data.MoviesDataSource
import com.joss.tmdbapp.ui.ViewModelFactory
import dagger.Module
import dagger.Provides

@Module
object ViewModelModule {
    @JvmStatic
    @Provides
    fun provideViewModelFactory(movieService: MovieService) = ViewModelFactory(movieService)
}