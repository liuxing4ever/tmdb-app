package com.joss.tmdbapp.di


import com.joss.tmdbapp.BuildConfig
import com.joss.tmdbapp.api.MovieService
import com.joss.tmdbapp.api.MovieServiceFactory
import com.joss.tmdbapp.data.MoviesDataSource
import com.joss.tmdbapp.data.MoviesRepository
import dagger.Provides
import dagger.Module
import javax.inject.Singleton


@Module
class NetModule {

    @Provides
    @Singleton
    internal fun provideMovieService(): MovieService {
        return MovieServiceFactory.makeMoviesService(BuildConfig.DEBUG)
    }

}