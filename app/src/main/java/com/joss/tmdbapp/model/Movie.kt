package com.joss.tmdbapp.model

import com.google.gson.annotations.SerializedName

data class Movie(
        var fromSearch: Boolean = false,
        @SerializedName("id")val id: String = "",
        @SerializedName("title") val title: String = "",
        @SerializedName("overview")val overview: String = "",
        @SerializedName("poster_path")val posterPath: String? = "",
        @SerializedName("release_date")val releaseDate: String = "",
        @SerializedName("backdrop_path")val backdropPath: String? = ""
)