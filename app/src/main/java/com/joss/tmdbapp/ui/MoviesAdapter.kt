package com.joss.tmdbapp.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import com.joss.tmdbapp.model.Movie
import android.view.ViewGroup
import com.joss.tmdbapp.R

/**
 * Adapter for the list of movies.
 */
class MoviesAdapter : PagedListAdapter<Movie, RecyclerView.ViewHolder>(MOVIE_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_movie -> MovieViewHolder.create(parent)
            R.layout.item_movie_from_search -> MovieFromSearchViewHolder.create(parent)
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movie = getItem(position) ?: return
        when (getItemViewType(position)) {
            R.layout.item_movie_from_search -> (holder as MovieFromSearchViewHolder).bind(movie)
            R.layout.item_movie -> (holder as MovieViewHolder).bind(movie)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val movie = getItem(position)
        return if (movie != null && movie.fromSearch) {
            R.layout.item_movie_from_search
        } else {
            R.layout.item_movie
        }
    }

    companion object {
        private val MOVIE_COMPARATOR = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
                    oldItem == newItem
        }
    }
}