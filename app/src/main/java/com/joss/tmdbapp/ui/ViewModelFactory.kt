package com.joss.tmdbapp.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.os.Handler
import android.os.Looper
import com.joss.tmdbapp.api.MovieService
import com.joss.tmdbapp.data.MoviesDataSource
import com.joss.tmdbapp.data.MoviesRepository
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject

class ViewModelFactory @Inject constructor(
        private val movieService: MovieService) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MoviesViewModel(MoviesRepository(movieService, createBackgroundExecutor())) as T
    }

    private fun createBackgroundExecutor() = Executors.newFixedThreadPool(3)
}