package com.joss.tmdbapp.ui

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.joss.tmdbapp.R
import com.joss.tmdbapp.api.getImageUrlString
import com.joss.tmdbapp.model.Movie
import com.squareup.picasso.Picasso
import org.jetbrains.anko.toast

class MovieFromSearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val title: TextView = view.findViewById(R.id.tv_movieTitle)
    private val image: ImageView = view.findViewById(R.id.iv_moviePoster)
    private val overview: TextView = view.findViewById(R.id.tv_movieOverview)

    private var movie: Movie? = null

    companion object {
        fun create(parent: ViewGroup): MovieFromSearchViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_movie_from_search, parent, false)
            return MovieFromSearchViewHolder(view)
        }
    }

    init {
        view.setOnClickListener {
            it.context.toast("Go for it! 🌠")
        }
    }

    fun bind(movie: Movie?) {
        if (movie == null) {
            showLoadingState()
        } else {
            showMovieData(movie)
        }
    }

    private fun showLoadingState() {
        val resources = itemView.resources
        title.text = resources.getString(R.string.loading)
        image.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.loading))
    }

    private fun showMovieData(movie: Movie) {
        this.movie = movie
        val releaseYear = movie.releaseDate.split("-")[0]
        title.text = itemView.context.getString(R.string.search_title_with_year, movie.title, releaseYear)
        overview.text = movie.overview
        if (movie.posterPath.isNullOrEmpty()) {
            image.setImageBitmap(null)
        } else {
            displayMovieImage(movie.posterPath!!)
        }
    }

    private fun displayMovieImage(imgPath: String) {
        Picasso.get()
                .load(getImageUrlString(imgPath))
                .fit()
                .centerCrop()
                .into(image)
    }
}