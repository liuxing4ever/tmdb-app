package com.joss.tmdbapp.ui

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.joss.tmdbapp.R
import com.joss.tmdbapp.api.getImageUrlString
import com.joss.tmdbapp.model.Movie
import com.squareup.picasso.Picasso
import org.jetbrains.anko.toast

class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val title: TextView = view.findViewById(R.id.tv_movieTitle)
    private val image: ImageView = view.findViewById(R.id.iv_moviePoster)

    private var movie: Movie? = null

    init {
        view.setOnClickListener {
            it.context.toast("You should totally watch it! 🤓")
        }
    }

    fun bind(movie: Movie?) {
        if (movie == null) {
            showLoadingState()
        } else {
            showMovieData(movie)
        }
    }

    private fun showLoadingState() {
        val resources = itemView.resources
        title.text = resources.getString(R.string.loading)
        image.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.loading))
    }

    private fun showMovieData(movie: Movie) {
        this.movie = movie
        title.text = movie.title
        if (movie.backdropPath.isNullOrEmpty()) {
            image.setImageBitmap(null)
        } else {
            displayMovieImage(movie.backdropPath!!)
        }
    }

    private fun displayMovieImage(imgPath: String) {
        Picasso.get()
                .load(getImageUrlString(imgPath))
                .fit()
                .centerCrop()
                .into(image)
    }

    companion object {
        fun create(parent: ViewGroup): MovieViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_movie, parent, false)
            return MovieViewHolder(view)
        }
    }
}