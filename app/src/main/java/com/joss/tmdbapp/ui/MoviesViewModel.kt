package com.joss.tmdbapp.ui

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.joss.tmdbapp.data.MoviesRepository


class MoviesViewModel(repository: MoviesRepository) : ViewModel() {

    private val query = MutableLiveData<String>()
    private val moviesResult = Transformations.map(query) {
        repository.getMovies(it)
    }

    val moviesPagedList = Transformations.switchMap(moviesResult) { it.pagedList }!!
    val initialLoad = Transformations.switchMap(moviesResult) { it.initialLoad }!!

    init {
        searchMoviesWithString("")
    }

    fun searchMoviesWithString(newQuery: String) {
        query.postValue(newQuery)
    }
}