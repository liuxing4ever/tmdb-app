package com.joss.tmdbapp.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import com.joss.tmdbapp.R
import com.joss.tmdbapp.TmdbApplication
import com.joss.tmdbapp.data.Status
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import android.app.SearchManager
import android.arch.paging.PagedList
import android.content.Context
import android.support.v7.widget.SearchView
import android.widget.Toast
import com.joss.tmdbapp.model.Movie


class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: MoviesViewModel
    private val adapter = MoviesAdapter()
    private var mToast: Toast? = null

    // =============
    // Lifecycle
    // =============

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.setTitle(R.string.activity_title)

        (application as TmdbApplication).appComponent.inject(this)
        viewModel = getViewModel()

        initAdapter()
        setupObservers()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnCloseListener(onCloseSearch)
        searchView.setOnQueryTextListener(this)
        searchView.queryHint = getString(R.string.search_hint)

        return true
    }

    // =============
    // Actions
    // =============

    override fun onQueryTextChange(p0: String?): Boolean {
        p0?.let {
            viewModel.searchMoviesWithString(p0)
        }
        return true
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    // =============
    // Private
    // =============

    private fun getViewModel(): MoviesViewModel {
        return ViewModelProviders.of(this, viewModelFactory)
                .get(MoviesViewModel::class.java)
    }

    private fun initAdapter() {
        rv_movies.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.initialLoad.observe(this, Observer {
            showLoadingAnimation(it?.status == Status.RUNNING)
            if (it?.status == Status.FAILED) {
                it.msg?.let {
                    showToast(it)
                    showNoResultsMessage(true)
                }
            } else {
                showNoResultsMessage(false)
            }
        })
        viewModel.moviesPagedList.observe(this, Observer<PagedList<Movie>> {
            adapter.submitList(it)
        })
    }

    private val onCloseSearch = SearchView.OnCloseListener {
        showToast("close")
        viewModel.searchMoviesWithString("")
        false
    }

    private fun showLoadingAnimation(show: Boolean) {
        pb_loading.visibility = if (show) View.VISIBLE
                                else View.GONE
    }

    private fun showNoResultsMessage(show: Boolean) {
        tv_noResults.visibility = if (show) View.VISIBLE
                                  else View.GONE
    }

    private fun showToast(msg: String) {
        mToast?.cancel()
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT)
        mToast?.show()
    }
}
