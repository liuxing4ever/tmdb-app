package com.joss.tmdbapp

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import com.joss.tmdbapp.api.MovieResponse
import com.joss.tmdbapp.api.MovieService
import com.joss.tmdbapp.data.Listing
import com.joss.tmdbapp.data.MoviesDataSource.Companion.INITIAL_LOAD_FAILURE
import com.joss.tmdbapp.data.MoviesRepository
import com.joss.tmdbapp.data.NetworkState
import com.joss.tmdbapp.model.Movie
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test

import retrofit2.mock.Calls

import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito.`when`

import java.util.concurrent.Executor
import org.mockito.MockitoAnnotations
import org.junit.Before
import java.io.IOException


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

class MovieRepositoryTest {

    @get:Rule // make all live data calls synchronous
    val instantExecutor = InstantTaskExecutorRule()

    @Mock
    private val movieService: MovieService? = null

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun fetchValidData_shouldLoadIntoPagedList() {

        // Make the mocked movieService return a dummy list of 3 movies
        val dummyMovies = listOf(Movie(), Movie(), Movie())
        val response = MovieResponse(dummyMovies, 1)
        `when`(movieService?.getPopularMoviesPage(1))
                .thenReturn(Calls.response(response))

        val networkExecutor = Executor { command -> command.run() }
        val repository = MoviesRepository(movieService!!, networkExecutor)

        // Check that the PagedList used to display the movies contains the dummy list
        val listing = repository.getMovies("")
        MatcherAssert.assertThat(getPagedList(listing),
                CoreMatchers.`is`(dummyMovies))
    }

    @Test
    fun failToLoadInitial_returnsProperError() {

        // Make the mocked movieService popular movies call fail
        `when`(movieService?.getPopularMoviesPage(1))
                .thenReturn(Calls.failure(IOException("")))

        val networkExecutor = Executor { command -> command.run() }
        val repository = MoviesRepository(movieService!!, networkExecutor)

        val listing = repository.getMovies("")

        // trigger load & check that the initial load state is FAILED
        getPagedList(listing)
        MatcherAssert.assertThat(getInitialLoadState(listing),
                CoreMatchers.`is`(NetworkState.error(INITIAL_LOAD_FAILURE)))
    }

    private fun getPagedList(listing: Listing<Movie>): PagedList<Movie> {
        val observer = LoggingObserver<PagedList<Movie>>()
        listing.pagedList.observeForever(observer)
        MatcherAssert.assertThat(observer.value, CoreMatchers.`is`(CoreMatchers.notNullValue()))
        return observer.value!!
    }

    private fun getInitialLoadState(listing: Listing<Movie>) : NetworkState? {
        val networkObserver = LoggingObserver<NetworkState>()
        listing.initialLoad.observeForever(networkObserver)
        return networkObserver.value
    }

    private class LoggingObserver<T> : Observer<T> {
        var value : T? = null
        override fun onChanged(t: T?) {
            this.value = t
        }
    }

}
